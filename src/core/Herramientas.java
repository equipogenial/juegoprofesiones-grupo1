package core;

public class Herramientas {

	private int durabilidad;
	private int precio;
	private String TipoHerramienta;
	
	public int getDurabilidad() {
		return durabilidad;
	}
	public void setDurabilidad(int durabilidad) {
		this.durabilidad = durabilidad;
	}
	public int getPrecio() {
		return precio;
	}
	public void setPrecio(int precio) {
		this.precio = precio;
	}
	public String getTipoInstrumento() {
		return TipoHerramienta;
	}
	public void setTipoHerramienta(String tipoHerramienta) {
		TipoHerramienta = tipoHerramienta;
	}
	
}
