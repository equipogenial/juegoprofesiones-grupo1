package core;

import java.util.Random;
import java.util.Scanner;

import core.Teclado;

public class Player {

	private int vida = 100;
	private int dinero = 20;
	private int hambre = 100;
	Herramientas herramienta;
	Profession ProfesionActiva;
	public Player(int vida, int dinero, int hambre, Herramientas herramienta, Profession profesionActiva) {

		this.vida = vida;
		this.dinero = dinero;
		this.hambre = hambre;
		this.herramienta = herramienta;
		this.ProfesionActiva = profesionActiva;
		

	}
	



	public Player(int vida, int dinero) {

	}

	public int getVida() {
		return vida;
	}

	public void setVida(int vida) {
		this.vida = vida;
	}

	public int getDinero() {
		return dinero;
	}

	public void setDinero(int dinero) {
		this.dinero = dinero;
	}

	public int getHambre() {
		return hambre;
	}

	public void setHambre(int hambre) {
		this.hambre = hambre;
	}

	public Herramientas getInstrumento() {
		return herramienta;
	}

	public void setInstrumento(Herramientas herramienta) {
		this.herramienta = herramienta;
	}

	public Profession getProfesionActiva() {
		return ProfesionActiva;
	}

	public void setProfesionActiva(Profession profesionActiva) {
		ProfesionActiva = profesionActiva;
	}

	public void aprenderProfesion() {

		
		Pantalla.menuAprenderProfesion();

		int opcion;
		opcion =Teclado.scan3();
		
		switch (opcion) {
		case 1:

			Profession agricultor = new Agricultor();
			this.dinero = this.dinero - agricultor.getCosteProf();
			this.setProfesionActiva(agricultor);
			break;
		case 2:
			Profession pescador = new Pescador();
			this.dinero = this.dinero - pescador.getCosteProf();
			this.setProfesionActiva(pescador);

			break;
		case 3:
			Profession cazador = new Cazador();
			this.dinero = this.dinero - cazador.getCosteProf();
			this.setProfesionActiva(cazador);
			break;

		}

	}

	public void comprarInstrumento() {
		
		int opcion = 0;
		Pantalla.menuComprarinstrumento();
		
		opcion =Teclado.scan3();
		switch (opcion) {
		case 1:
			Herramientas hoz = new Hoz();
			this.herramienta = hoz;
			this.dinero = this.dinero - hoz.getPrecio();
			

			break;
		case 2:
			Herramientas ca�a = new Ca�aDePescar();
			this.herramienta = ca�a;
			this.dinero = this.dinero - ca�a.getPrecio();
			
			break;
		case 3:
			Herramientas escopeta = new Escopeta();
			this.herramienta = escopeta;
			this.dinero = this.dinero - escopeta.getPrecio();
			
			break;

		}
	}

	public void Dormir() {

		Random rnd = new Random();
		int aleatorio = 0;
		Scanner teclado = new Scanner(System.in);
		Pantalla.menuDormir(this);
		int opcion = 0;
		opcion =Teclado.scan2();
		if (opcion == 1) {
			this.vida = this.vida + 1;
			aleatorio = rnd.nextInt(1) + 1;
			this.dinero = this.dinero - aleatorio;
		}
		if (opcion == 2) {
			this.vida = this.vida - 5;
		}

	}

	public void Trabajar() {
		Random rnd = new Random();
		int aleatorio = 0;
		aleatorio = rnd.nextInt(10);
		this.herramienta.setDurabilidad(this.herramienta.getDurabilidad() - aleatorio);

		int suerte = rnd.nextInt(1) + 1;
		int fortuna = rnd.nextInt(this.herramienta.getDurabilidad())+1;
		this.dinero = this.dinero + (suerte * fortuna);
	}
	public void comprarComida() {
		
		int dinero;
		int comida;
		
		Random aleatorio = new Random();
		
		comida=aleatorio.nextInt(10)+5;
		dinero=aleatorio.nextInt(15)+5;
		
		this.dinero=this.dinero-dinero;
		this.hambre=this.hambre+comida;
		
		
		
	}
	public void mostrarEstado() {
		System.out.println("Vida: "+this.vida); 
		System.out.println("Dinero: "+this.dinero); 
		System.out.println("Hambre: "+this.hambre); 
		System.out.println("Instrumento: "+getInstrumento().getTipoInstrumento());
		System.out.println("Durabilidad del instrumento: "+getInstrumento().getDurabilidad());
		System.out.println("Profesion: "+getProfesionActiva().getTipoDeProfesion()); 
		System.out.println("\n"); 
	}
}
