package core;

public class Pescador implements Profession {


	public Pescador() {

	}
	private int Coste=20;
	private String TipoDeProfesion="Pescador";
	public int getCosteProf() {

		return this.Coste ;
	}
	
	public void setCosteProf(int costeDeProfesion) {
		Coste = costeDeProfesion;
	}

	public String getTipoDeProfesion() {
		return TipoDeProfesion;
	}

	public void setTipoDeProfesion(String tipoDeProfesion) {
		TipoDeProfesion = tipoDeProfesion;
	}
}
