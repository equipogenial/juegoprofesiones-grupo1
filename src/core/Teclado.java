package core;

import java.util.InputMismatchException;
import java.util.Scanner;

import core.Player;

public class Teclado {

	public static int escaneo() {

		Scanner scn = new Scanner(System.in);

		int opcion = 0;
		try {
			opcion = scn.nextInt();

		} catch (InputMismatchException error) {
			System.out.println("Debe introducir un valor correcto");
		}

		return opcion;

	}

	public static int scan3() {

		Scanner scn = new Scanner(System.in);
		int opcion = 0;
		try {
			opcion = scn.nextInt();

		} catch (InputMismatchException error) {
			System.out.println("Debe introducir un valor correcto");
		}

		return opcion;

	}

	public static int scan2() {

		Scanner scn = new Scanner(System.in);
		int opcion = 0;
		try {
			opcion = scn.nextInt();

		} catch (InputMismatchException error) {
			System.out.println("Debe introducir un valor correcto");
		}

		return opcion;

	}

	public static String scanNombre() {

		Scanner scn = new Scanner(System.in);

		String opcion = scn.nextLine();

		return opcion;
	}

}
