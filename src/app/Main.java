package app;

import java.util.Scanner;

import core.Agricultor;
import core.Cazador;
import core.CañaDePescar;
import core.Escopeta;
import core.Hoz;
import core.Herramientas;
import core.Player;
import core.Pantalla;
import core.Pescador;
import core.Profession;
import core.Teclado;

public class Main {

	public static void main(String[] args) {


		System.out.println("Escriba el nombre del jugador");

		String nombre = Teclado.scanNombre();
	
		Profession p = new Agricultor();
		Herramientas i = new Herramientas();
		Player jugador = new Player(50, 25, 100, i, p);
		int opcion = 0;
		int contador = 0;
		int dias = 1;


		while (jugador.getVida() > 0) {
			System.out.println("Nombre: " + nombre);
			System.out.println(jugador.getInstrumento().getTipoInstrumento());

		
			
		

			


				

				Pantalla.mostrarOpciones(jugador,contador,dias);


				opcion = Teclado.escaneo();
			
			if (opcion == 1) {

				jugador.comprarComida();
				System.out.println(jugador.getDinero() + "\n" + jugador.getHambre());

			}
			if (opcion == 2) {

				jugador.comprarInstrumento();

			}

			if (opcion == 3) {

				jugador.aprenderProfesion();

			}

			if (opcion == 4) {

				jugador.Dormir();

				contador = 0;

				dias++;

			}

			if (opcion == 5
					&& (jugador.getProfesionActiva() instanceof Agricultor && jugador.getInstrumento() instanceof Hoz
							|| jugador.getProfesionActiva() instanceof Pescador
									&& jugador.getInstrumento() instanceof CañaDePescar
							|| jugador.getProfesionActiva() instanceof Cazador && jugador.getInstrumento() instanceof Escopeta)
					&& contador < 1) {

				jugador.Trabajar();

				System.out.println(jugador.getInstrumento().getDurabilidad());
				contador++;
				System.out.println(jugador.getDinero());

			} else if (opcion == 5) {

				if (jugador.getInstrumento().getTipoInstrumento() == null) {
					System.out.println("Todavía no tienes instrumento");
				} else if (jugador.getInstrumento().getDurabilidad() <= 0) {
					System.out.println("No puede trabajar: tu herramienta esta rota, y no se puede reparar");
				} else if(contador>0) {
					System.out.println("No puede trabajar: Ya has trabajado");
				}else {
					System.out.println("No puede trabajar: tu herramienta no es la adecuada para este trabajo");
				}


				System.out.println("No puede trabajar: o su herramienta no es adecuada para su profesion,\n "
						+ "o no tiene herramienta");

			}
			jugador.mostrarEstado();
			opcion = 0;
			

		}
		System.out.println("Ha muerto: " +nombre+"\n"+"A los: "+dias+" dias");
		
	}

}
